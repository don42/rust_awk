use crate::ast::{Eval, Value};
use crate::state::{ProgramState, State};

#[derive(Debug)]
pub struct AddOperation {
    left: Box<dyn Eval>,
    right: Box<dyn Eval>,
}

impl AddOperation {
    pub fn new(left: Box<dyn Eval>, right: Box<dyn Eval>) -> Self {
        AddOperation { left, right }
    }

    pub fn from_plain(left: f64, right: f64) -> Self {
        AddOperation {
            left: Box::new(Value::Number(left)),
            right: Box::new(Value::Number(right)),
        }
    }
}

impl Eval for AddOperation {
    fn eval(&self, state: &mut Box<dyn ProgramState>) -> Value {
        let left = self.left.eval(state);
        let right = self.right.eval(state);
        Value::Number(left.to_number() + right.to_number())
    }
}

#[derive(Debug)]
pub struct SubOperation {
    left: Box<dyn Eval>,
    right: Box<dyn Eval>,
}

impl Eval for SubOperation {
    fn eval(&self, state: &mut Box<dyn ProgramState>) -> Value {
        let left = self.left.eval(state);
        let right = self.right.eval(state);
        Value::Number(left.to_number() - right.to_number())
    }
}

#[derive(Debug)]
pub struct MulOperation {
    left: Box<dyn Eval>,
    right: Box<dyn Eval>,
}

impl Eval for MulOperation {
    fn eval(&self, state: &mut Box<dyn ProgramState>) -> Value {
        let left = self.left.eval(state);
        let right = self.right.eval(state);
        Value::Number(left.to_number() * right.to_number())
    }
}

#[derive(Debug)]
pub struct DivOperation {
    left: Box<dyn Eval>,
    right: Box<dyn Eval>,
}

impl Eval for DivOperation {
    fn eval(&self, state: &mut Box<dyn ProgramState>) -> Value {
        let left = self.left.eval(state);
        let right = self.right.eval(state);
        Value::Number(left.to_number() / right.to_number())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_simple_add() {
        let mut state: Box<dyn ProgramState> = Box::new(State::new());
        let result = AddOperation::from_plain(5.0, 1.0).eval(&mut state);
        assert_eq!(result, Value::Number(6.0));
    }
}
