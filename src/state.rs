use std::collections::HashMap;

use crate::ast::Value;

pub trait ProgramState {
    fn read_variable(&mut self, identifier: String) -> Value;
    fn write_variable(&mut self, identifier: String, value: Value);

    fn read_field(&self, index: i16) -> Value;
    fn write_field(&mut self, index: i16, value: Value);

    fn set_next_line(&mut self, line_state: LineState);
}

pub struct LineState {
    pub line: String,
    fields: Vec<String>,
    changed: bool,
}

impl LineState {
    pub fn new(line: String) -> Self {
        let fields: Vec<String> = line
            .split("\t")
            .map(|x| x.trim())
            .map(|x| x.to_string())
            .collect();

        LineState {
            line,
            fields,
            changed: false,
        }
    }

    pub fn read_field(&self, index: i16) -> Value {
        if index > 0 {
            let index = index as usize;
            Value::Str(self.fields[index - 1].clone())
        } else if index == 0 {
            Value::Str(self.to_string())
        } else {
            panic!("field index out of bounds");
        }
    }

    fn to_string(&self) -> String {
        if self.changed == false {
            self.line.to_string()
        } else {
            self.fields.join(" ")
        }
    }
}

pub struct State {
    pub vars: HashMap<String, Value>,
    current_line_state: Option<LineState>,
}

impl State {
    pub fn new() -> Self {
        State {
            vars: HashMap::new(),
            current_line_state: None,
        }
    }
}

impl ProgramState for State {
    fn read_field(&self, index: i16) -> Value {
        if let Some(ref line) = &self.current_line_state {
            line.read_field(index)
        } else {
            panic!("no line found")
        }
    }

    fn write_field(&mut self, index: i16, value: Value) {}

    fn read_variable(&mut self, identifier: String) -> Value {
        self.vars.entry(identifier).or_default().clone()
    }

    fn write_variable(&mut self, identifier: String, value: Value) {
        self.vars.insert(identifier, value);
    }

    fn set_next_line(&mut self, line_state: LineState) {
        self.current_line_state = Some(line_state);
    }
}
