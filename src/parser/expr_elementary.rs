use nom::branch::alt;
use nom::character::complete::{alpha1, alphanumeric0};
use nom::combinator::{map, recognize};
use nom::sequence::pair;
use nom::IResult;

use super::constants::{parse_constant, Constant};
//use super::expression::Expression;

#[derive(Debug, Clone, PartialEq)]
pub enum ElementaryExpression {
    Constant(Constant),
    Variable(String),
    //Field(Expression),
    //Parathesized(Expression),
    //Function{ ident: String, expr: Expression}
}

pub fn parse_elementary_expression(i: &str) -> IResult<&str, ElementaryExpression> {
    alt((
        parse_variable,
        map(parse_constant, |s: Constant| {
            ElementaryExpression::Constant(s)
        }),
    ))(i)
}

fn parse_variable(i: &str) -> IResult<&str, ElementaryExpression> {
    let parser = recognize(pair(alpha1, alphanumeric0));
    map(parser, |s: &str| {
        ElementaryExpression::Variable(s.to_owned())
    })(i)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_named_var() {
        assert_eq!(
            parse_elementary_expression("d0reason"),
            Ok(("", ElementaryExpression::Variable("d0reason".to_owned()))),
        );
        assert_eq!(
            parse_elementary_expression("d0asdon"),
            Ok(("", ElementaryExpression::Variable("d0asdon".to_owned()))),
        );
    }

    #[test]
    fn test_str_constant() {
        assert_eq!(
            parse_elementary_expression("\"constant value\""),
            Ok((
                "",
                ElementaryExpression::Constant(Constant::Str("constant value".to_owned()))
            )),
        );
        assert_eq!(
            parse_elementary_expression("\"test\\ttest\""),
            Ok((
                "",
                ElementaryExpression::Constant(Constant::Str("test\ttest".to_owned()))
            )),
        );
    }

    #[test]
    fn test_num_constant() {
        assert_eq!(
            parse_elementary_expression("15.1"),
            Ok(("", ElementaryExpression::Constant(Constant::Number(15.1)))),
        );
    }
}
