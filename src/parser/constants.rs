use nom::branch::alt;
use nom::bytes::complete::{escaped_transform, is_not};
use nom::character::complete::char;
use nom::sequence::delimited;
use nom::IResult;

#[derive(Debug, Clone, PartialEq)]
pub enum Constant {
    Str(String),
    Number(f64),
}

pub fn parse_constant(i: &str) -> IResult<&str, Constant> {
    alt((parse_string_constant, parse_number_constant))(i)
}

fn parse_string_content(i: &str) -> IResult<&str, String> {
    let mut parser = escaped_transform(
        is_not("\\\r\n\""),
        '\\',
        alt((
            nom::combinator::recognize(char('\\')),
            nom::combinator::recognize(char('\"')),
            nom::combinator::value("\r", char('r')),
            nom::combinator::value("\n", char('n')),
            nom::combinator::value("\t", char('t')),
        )),
    );
    parser(i)
}

fn parse_string_constant(i: &str) -> IResult<&str, Constant> {
    let (i, content) = delimited(char('"'), parse_string_content, char('"'))(i)?;

    Ok((i, Constant::Str(String::from(content))))
}

fn parse_number_constant(i: &str) -> IResult<&str, Constant> {
    let (i, value) = nom::number::complete::double(i)?;
    Ok((i, Constant::Number(value)))
}

#[cfg(test)]
mod tests {
    use super::Constant;

    #[test]
    fn test_simple_string() {
        let s = "\"this is a test.\\n\"";
        assert_eq!(
            super::parse_constant(s),
            Ok(("", Constant::Str("this is a test.\n".to_owned()),)),
        );
    }

    #[test]
    fn test_string_with_quotes() {
        let s = "\"this is a \\\"test\\\".\\t\"";
        assert_eq!(
            super::parse_constant(s),
            Ok(("", Constant::Str("this is a \"test\".\t".to_owned()),)),
        );
    }

    #[test]
    fn test_number_zero() {
        assert_eq!(super::parse_constant("0"), Ok(("", Constant::Number(0.0))),)
    }

    #[test]
    fn test_number_exponent() {
        assert_eq!(
            super::parse_constant("3.14e2"),
            Ok(("", Constant::Number(314.0))),
        )
    }

    #[test]
    fn test_number_exponent_zero() {
        assert_eq!(
            super::parse_constant("1.0E0"),
            Ok(("", Constant::Number(1.0e0))),
        )
    }
}
