# Design of the Parser

## Status
Status: accepted

## Context
Most examples for the Nom parser generator implement a single pass parser.
Most academic sources seems to split this into the lexical analysis step and the parsing step.
It is unclear how far these lessons apply to writing parsers in nom as it's much more robust using
nom and the rust standard library than doing string manipulation by hand.

## Decision

*Compilers --- Principles, Techniques, & Tools* lists three advantages for using a separate lexical analysis step.
1. Simpler design by only operating on a sanitized token stream in the parsing step.
2. Improved compiler efficiency by optimizing the steps independently.
3. Enhhanced compiler portability by isolating input-device-specific peculiarities in the lexical analyser.

Point 2. and 3. are not relevant for this project. Compiler efficiency will not be an issue because this is mostly
a learning project and Rust is generally optimized enough for general tasks.
Compiler portability also shouldn't be an issue because encoding, line-endings, and file reading are already handled
by the Rust standard library and only UTF-8 files being allowed.

Simpler compiler design would be useful but at the cost of effort to implement the two separate steps and the interface
between them. Nom/Rust already allow quite a lot of useful abstractions in the parser functions which should keep
them simple enough.

## Consequences

The implementation should be easier to write in the short term, but there might be maintainability issues
long term due to a lack of separation.
