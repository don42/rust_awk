use std::fmt;

use crate::state::ProgramState;

mod assignments;
pub use self::assignments::{Assignment, AssignmentLeft};
mod arithmetic;
pub use self::arithmetic::*;

const AWK_FALSE: Value = Value::Number(0.0);
const AWK_TRUE: Value = Value::Number(1.0);

pub trait Eval: std::fmt::Debug {
    fn eval(&self, state: &mut Box<dyn ProgramState>) -> Value;
}

#[derive(Clone, Debug)]
pub enum Value {
    Str(String),
    Number(f64),
}

impl Value {
    pub fn to_bool(&self) -> bool {
        match self {
            &Value::Str(ref x) => x != "",
            &Value::Number(x) => x != 0.0,
        }
    }

    pub fn to_number(&self) -> f64 {
        match self {
            &Value::Number(x) => x,
            &Value::Str(ref x) => x.parse::<f64>().unwrap_or(0.0),
        }
    }

    pub fn to_string(&self) -> String {
        match self {
            &Value::Number(x) => format!("{}", x),
            &Value::Str(ref x) => x.clone(),
        }
    }
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

impl Default for Value {
    fn default() -> Value {
        Value::Str("".to_string())
    }
}

impl Eval for Value {
    fn eval(&self, _state: &mut Box<dyn ProgramState>) -> Value {
        self.clone()
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        use self::Value::*;

        fn cmp_with_cast(sval: &String, nval: f64) -> bool {
            if sval.is_empty() {
                return false;
            }
            eprintln!("{:?}, {:?}", sval, nval);
            sval.parse::<f64>().map_or(false, |x| x == nval)
        }

        match (self, other) {
            (&Str(ref lval), &Str(ref rval)) => lval == rval,
            (&Number(lval), &Number(rval)) => lval == rval,
            (&Str(ref sval), &Number(nval)) => cmp_with_cast(sval, nval),
            (&Number(nval), &Str(ref sval)) => cmp_with_cast(sval, nval),
        }
    }
}

#[derive(Debug)]
pub struct Variable {
    identifier: String,
}

impl Eval for Variable {
    fn eval(&self, state: &mut Box<dyn ProgramState>) -> Value {
        state.read_variable(self.identifier.clone())
    }
}

#[derive(Debug)]
pub struct Field {
    identifier: Box<dyn Eval>,
}

impl Field {
    pub fn new(identifier: Box<dyn Eval>) -> Self {
        Field { identifier }
    }
}

impl Eval for Field {
    fn eval(&self, state: &mut Box<dyn ProgramState>) -> Value {
        let value = self.identifier.eval(state);
        let index: i16 = match value {
            Value::Number(x) => x,
            Value::Str(ref x) => x.parse().unwrap_or(0.0),
        }
        .trunc() as i16;
        if index.is_negative() {
            panic!("index is negative: {:?}", value);
        } else {
            state.read_field(index)
        }
    }
}

#[derive(Debug)]
pub struct Equals {
    left: Box<dyn Eval>,
    right: Box<dyn Eval>,
}

impl Equals {
    pub fn new(left: Box<dyn Eval>, right: Box<dyn Eval>) -> Self {
        Equals { left, right }
    }
}

impl Eval for Equals {
    fn eval(&self, state: &mut Box<dyn ProgramState>) -> Value {
        let left = self.left.eval(state);
        let right = self.right.eval(state);
        if left == right {
            AWK_TRUE
        } else {
            AWK_FALSE
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! eq_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (l, r) = $value;
                    assert_eq!(l, r);
                }
        )*
        }
    }

    #[test]
    fn constant_not_equals() {
        assert_ne!(AWK_TRUE, AWK_FALSE);
    }

    eq_tests! {
        eq_1: (Value::Number(0.0), Value::Number(0.0)),
        eq_2: (Value::Number(0.0), Value::Str("0.0".to_string())),
        eq_3: (Value::Number(0.0), Value::Str("0".to_string())),
    }
}
