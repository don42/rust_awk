use nom::branch::alt;
use nom::character::complete::{char, multispace0};
use nom::combinator::{map, opt};
use nom::multi::fold_many0;
use nom::sequence::{pair, preceded};
use nom::IResult;

use super::constants::Constant;
use super::expr_elementary::ElementaryExpression;
use super::expr_primitive::{parse_primitive_expression, PrimitiveExpression};
use super::util;

#[derive(Debug, Clone, PartialEq)]
pub enum Expression {}

#[derive(Debug, Clone, PartialEq)]
struct ConcatTerm {
    pub head: AddTerm,
    pub tail: Vec<AddTerm>,
}

impl ConcatTerm {
    pub fn new(head: AddTerm) -> Self {
        ConcatTerm {
            head,
            tail: Vec::new(),
        }
    }
}

fn parse_concat(i: &str) -> IResult<&str, ConcatTerm> {
    let mut parser = preceded(multispace0, parse_addition);
    let (i, head) = parser(i)?;
    fold_many0(
        parser,
        ConcatTerm::new(head),
        |mut term: ConcatTerm, item| {
            term.tail.push(item);
            term
        },
    )(i)
}

#[derive(Debug, Clone, PartialEq)]
enum AddTerm {
    Add { left: MulTerm, right: MulTerm },
    Sub { left: MulTerm, right: MulTerm },
    Simple(MulTerm),
}

fn parse_addition(i: &str) -> IResult<&str, AddTerm> {
    let parser = pair(
        util::spacey(parse_multiplication),
        opt(pair(
            alt((char('+'), char('-'))),
            util::spacey(parse_multiplication),
        )),
    );
    map(parser, |(left, rest)| match rest {
        None => AddTerm::Simple(left),
        Some(('+', right)) => AddTerm::Add { left, right },
        Some(('-', right)) => AddTerm::Sub { left, right },
        _ => unreachable!(),
    })(i)
}

#[derive(Debug, Clone, PartialEq)]
enum MulTerm {
    Mul {
        left: PrimitiveExpression,
        right: PrimitiveExpression,
    },
    Div {
        left: PrimitiveExpression,
        right: PrimitiveExpression,
    },
    Mod {
        left: PrimitiveExpression,
        right: PrimitiveExpression,
    },
    Simple(PrimitiveExpression),
}

fn parse_multiplication(i: &str) -> IResult<&str, MulTerm> {
    let parser = pair(
        util::spacey(parse_primitive_expression),
        opt(pair(
            alt((char('*'), char('/'), char('%'))),
            util::spacey(parse_primitive_expression),
        )),
    );
    map(parser, |(left, rest)| match rest {
        None => MulTerm::Simple(left),
        Some(('*', right)) => MulTerm::Mul { left, right },
        Some(('/', right)) => MulTerm::Div { left, right },
        Some(('%', right)) => MulTerm::Mod { left, right },
        _ => unreachable!(),
    })(i)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_multiplication() {
        assert_eq!(
            parse_multiplication("42*3"),
            Ok((
                "",
                MulTerm::Mul {
                    left: PrimitiveExpression {
                        operator: None,
                        expression: ElementaryExpression::Constant(Constant::Number(42.0))
                    },
                    right: PrimitiveExpression {
                        operator: None,
                        expression: ElementaryExpression::Constant(Constant::Number(3.0))
                    },
                }
            ))
        );
    }

    #[test]
    fn test_multiplication_with_spaces() {
        assert_eq!(
            parse_multiplication("42 * 3"),
            Ok((
                "",
                MulTerm::Mul {
                    left: PrimitiveExpression {
                        operator: None,
                        expression: ElementaryExpression::Constant(Constant::Number(42.0))
                    },
                    right: PrimitiveExpression {
                        operator: None,
                        expression: ElementaryExpression::Constant(Constant::Number(3.0))
                    },
                }
            ))
        );
    }

    #[test]
    fn test_multiplication_single_term() {
        assert_eq!(
            parse_multiplication("42"),
            Ok((
                "",
                MulTerm::Simple(PrimitiveExpression {
                    operator: None,
                    expression: ElementaryExpression::Constant(Constant::Number(42.0))
                },)
            ))
        );
    }

    #[test]
    fn test_addition_with_spaces() {
        let expected = AddTerm::Sub {
            left: MulTerm::Simple(PrimitiveExpression {
                operator: None,
                expression: ElementaryExpression::Constant(Constant::Number(42.0)),
            }),
            right: MulTerm::Mul {
                left: PrimitiveExpression {
                    operator: None,
                    expression: ElementaryExpression::Constant(Constant::Number(3.0)),
                },
                right: PrimitiveExpression {
                    operator: None,
                    expression: ElementaryExpression::Constant(Constant::Number(2.0)),
                },
            },
        };
        assert_eq!(parse_addition("42 - 3 * 2"), Ok(("", expected)),);
    }

    #[test]
    fn test_concat() {
        let result = parse_concat("42 23");
        let expected = ConcatTerm {
            head: AddTerm::Simple(MulTerm::Simple(PrimitiveExpression {
                operator: None,
                expression: ElementaryExpression::Constant(Constant::Number(42.0)),
            })),
            tail: vec![AddTerm::Simple(MulTerm::Simple(PrimitiveExpression {
                operator: None,
                expression: ElementaryExpression::Constant(Constant::Number(23.0)),
            }))],
        };
        assert!(result.is_ok());
        assert_eq!(result, Ok(("", expected)));
    }
}
