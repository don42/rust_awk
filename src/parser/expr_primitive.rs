use nom::branch::alt;
use nom::character::complete::char;
use nom::combinator::{map, opt, value};
use nom::sequence::pair;
use nom::IResult;

use super::constants::Constant;
use super::expr_elementary::{parse_elementary_expression, ElementaryExpression};
use super::util;

#[derive(Debug, Clone, PartialEq)]
pub struct PrimitiveExpression {
    pub operator: Option<UnaryOperator>,
    pub expression: ElementaryExpression,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum UnaryOperator {
    Minus,
    Plus,
    Not,
}

pub fn parse_primitive_expression(i: &str) -> IResult<&str, PrimitiveExpression> {
    let parser = pair(
        util::spacey(opt(alt((
            value(UnaryOperator::Minus, char('-')),
            value(UnaryOperator::Plus, char('+')),
            value(UnaryOperator::Not, char('!')),
        )))),
        util::spacey(parse_elementary_expression),
    );
    map(parser, |(operator, expr)| PrimitiveExpression {
        operator: operator,
        expression: expr,
    })(i)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_primitive_expression() {
        assert_eq!(
            parse_primitive_expression("-1"),
            Ok((
                "",
                PrimitiveExpression {
                    operator: Some(UnaryOperator::Minus),
                    expression: ElementaryExpression::Constant(Constant::Number(1.0)),
                }
            ))
        );
        assert_eq!(
            parse_primitive_expression("+1"),
            Ok((
                "",
                PrimitiveExpression {
                    operator: Some(UnaryOperator::Plus),
                    expression: ElementaryExpression::Constant(Constant::Number(1.0)),
                }
            ))
        );
        assert_eq!(
            parse_primitive_expression("!1"),
            Ok((
                "",
                PrimitiveExpression {
                    operator: Some(UnaryOperator::Not),
                    expression: ElementaryExpression::Constant(Constant::Number(1.0)),
                }
            ))
        );
        assert_eq!(
            parse_primitive_expression("1"),
            Ok((
                "",
                PrimitiveExpression {
                    operator: None,
                    expression: ElementaryExpression::Constant(Constant::Number(1.0)),
                }
            ))
        );
    }

    #[test]
    fn test_primitive_expression_with_spaces() {
        assert_eq!(
            parse_primitive_expression("- 1"),
            Ok((
                "",
                PrimitiveExpression {
                    operator: Some(UnaryOperator::Minus),
                    expression: ElementaryExpression::Constant(Constant::Number(1.0)),
                }
            ))
        );
        assert_eq!(
            parse_primitive_expression("+ 1"),
            Ok((
                "",
                PrimitiveExpression {
                    operator: Some(UnaryOperator::Plus),
                    expression: ElementaryExpression::Constant(Constant::Number(1.0)),
                }
            ))
        );
        assert_eq!(
            parse_primitive_expression("! 1"),
            Ok((
                "",
                PrimitiveExpression {
                    operator: Some(UnaryOperator::Not),
                    expression: ElementaryExpression::Constant(Constant::Number(1.0)),
                }
            ))
        );
        assert_eq!(
            parse_primitive_expression(" 1"),
            Ok((
                "",
                PrimitiveExpression {
                    operator: None,
                    expression: ElementaryExpression::Constant(Constant::Number(1.0)),
                }
            ))
        );
    }
}
