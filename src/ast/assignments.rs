use crate::ast::{Eval, Field, Value, Variable};
use crate::state::ProgramState;

#[derive(Debug)]
pub enum AssignmentLeft {
    Field(Field),
    Variable(Variable),
}

#[derive(Debug)]
pub struct Assignment {
    left: AssignmentLeft,
    right: Box<dyn Eval>,
}

impl Eval for Assignment {
    fn eval(&self, state: &mut Box<dyn ProgramState>) -> Value {
        let value = self.right.eval(state);
        match &self.left {
            AssignmentLeft::Field(_) => {}
            AssignmentLeft::Variable(var) => {
                state.write_variable(var.identifier.clone(), value.clone())
            }
        }
        value
    }
}
