use std::fs::File;
use std::io::prelude::BufRead;
use std::io::{self, BufReader};

mod ast;
use crate::ast::{Eval, Field, Value};
mod state;
use crate::state::{LineState, ProgramState, State};
mod parser;

struct Guard {
    expr: Box<dyn Eval>,
}

impl PatternMatch for Guard {
    fn matches(&self, state: &mut Box<dyn ProgramState>) -> bool {
        self.expr.eval(state).to_bool()
    }
}

trait PatternMatch {
    fn matches(&self, state: &mut Box<dyn ProgramState>) -> bool;
}

fn main() -> io::Result<()> {
    let file = File::open("./nasa_19950801.tsv")?;
    let reader = BufReader::new(file);

    let guard = Guard {
        expr: Box::new(ast::Equals::new(
            Box::new(Field::new(Box::new(Value::Number(6.0)))),
            Box::new(Value::Number(304.0)),
        )),
    };
    let mut state: Box<dyn ProgramState> = Box::new(State::new());

    for line in reader.lines() {
        state.set_next_line(LineState::new(line?.to_string()));

        if guard.matches(&mut state) {
            println!("{}", state.read_field(0));
        }
    }
    Ok(())
}
